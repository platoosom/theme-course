<?php
get_header("general");

while (have_posts()):
    the_post();
?>
	<!-- LIGHT SECTION -->
	<section class="lightSection clearfix pageHeader">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="page-title">
						<h2><?php the_title(); ?></h2>
					</div>
				</div>
				<div class="col-md-6">
					<ol class="breadcrumb float-right">
						<li>
							<a href="<?php bloginfo("url"); ?>">Home</a>
						</li>
						<li class="active"><?php the_title(); ?></li>
					</ol>
				</div>
			</div>
		</div>
	</section>

	<!-- MAIN CONTENT SECTION -->
	<section class="mainContent clearfix blogPage singleBlog">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div class="thumbnail">
								<img src="<?php echo get_the_post_thumbnail_url($post, "full"); ?>" alt="blog-image">
								<div class="caption">
									<div class="row">
										<div class="col-md-3 order-12">
											<h5>Details</h5>
											<ul class="list-unstyled">
												<li><i class="fa fa-user" aria-hidden="true"></i><?php the_author(); ?></li>
												<li><i class="fa fa-calendar" aria-hidden="true"></i><?php the_date("M d, Y");?></li>
												<li><i class="fa fa-tags" aria-hidden="true"></i><?php the_tags(); ?></li>
												<li><i class="fa fa-envelope" aria-hidden="true"></i><?php echo get_comments_number(); ?> comments</li>
											</ul>
										</div>
										<div class="col-md-9 order-1">
											<h3><?php the_title(); ?></h3>
											<?php the_content(); ?>
										</div>
									</div>
								</div>
								<ul class="pager">
									<li class="previous"><?php if(get_previous_posts_link()){?><?php echo get_previous_post_link("%link", "previous"); ?><?php }else{ echo " <a style='background-color: #f0f0f0;'></a>";}?></li>
									<li class="next float-right"><?php echo get_next_post_link("%link", "next")?></li>
								</ul>
								<?php
                                if(comments_open() || get_comments_number()){
                                    comments_template();
                                }
                                ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php
							$fields =  array(

								'author' =>
									'<div class="col-sm-6 col-12">
										<div class="form-group">
											<input type="text" class="form-control" id="" placeholder="Name" name="author">
										</div>
									</div>',

								'email' =>
									'<div class="col-sm-6 col-12">
										<div class="form-group">
											<input type="email" class="form-control" id="" placeholder="Email" name="email">
										</div>
									</div>',

								'url' =>
									'<div class="col-12">
										<div class="form-group">
											<input type="text" class="form-control" id="" placeholder="Website" name="url">
										</div>
									</div>',

								'cookies' =>
									'<div class="col-12">
                                        <p class="comment-form-cookies-consent">
                                            <input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes" />
                                            <label for="wp-comment-cookies-consent">' . __( 'Save my name, email, and website in this browser for the next time I comment.' ) . '</label>
                                        </p>
                                    </div>',
							);

							$comment_field = '
                                    <div class="col-12">
										<div class="form-group">
											<textarea class="form-control" rows="3" placeholder="Comment" name="comment" required="required"></textarea>
										</div>
									</div>';
							
                            /** Comment Form */
                            comment_form(array(
                                "fields" => $fields,
                                "comment_field" =>  $comment_field,
                                "class_submit" => "btn btn-primary",
                            ));
                            ?>
						</div>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php
    endwhile;
get_footer("general");
?>