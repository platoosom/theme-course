        <!-- FOOTER -->
        <?php
        get_template_part("template-part/footer")
        ?>

        <!-- COPY RIGHT -->
        <?php
        get_template_part("template-part/copyright")
        ?>

    </div>

    <!-- LOGIN MODAL -->
    <?php
    get_template_part("template-part/loginmodal")
    ?>

    <!-- SIGN UP MODAL -->
    <?php
    get_template_part("template-part/signupmodal")
    ?>

    <!-- PORDUCT QUICK VIEW MODAL -->
    <?php
    get_template_part("template-part/signupmodal")
    ?>

    <?php wp_footer(); ?>

    </body>
</html>

