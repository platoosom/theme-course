jQuery(document).ready(function ($) {

    $("#add-our-store-location").click(function(e){
        e.preventDefault();

        var date = new Date();
        var time = date.getTime();

        var data = '<tr>\n' +
        '                <td>\n' +
        '                    <label>Title:</label>\n' +
        '                    <input class="large-text" type="text" name="ourstorelocation['+time+'][title]" value="">\n' +
        '                </td>\n' +
        '                <td>\n' +
        '                    <label>Location:</label>\n' +
        '                    <textarea class="large-text" rows="3"  name="ourstorelocation['+time+'][location]"></textarea>\n' +
        '                </td>\n' +
        '                <td>\n' +
        '                    <a href="#" name="remove-our-store-location">-Remove</a>\n' +
        '                </td>'+
        '            </tr>';

        $("#our-store-location-body").append(data);
    });

    $("a[name=remove-our-store-location]").live("click", function(e){
        e.preventDefault();

        $(this).parent().parent().remove();
    });

    $("#add-our-people").click(function(e){
        e.preventDefault();

        var date = new Date();
        var time = date.getTime();

        var data = '<tr>\n' +
    '                    <td>\n' +
    '                        <label>Pic:</label>\n' +
    '                        <input class="large-text" type="text" name="ourpeoples['+time+'][pic]" value="">\n' +
    '                    </td>\n' +
    '                    <td>\n' +
    '                        <label>Name:</label>\n' +
    '                        <input type="text" class="large-text"  name="ourpeoples['+time+'][name]" value="">\n' +
    '                    </td>\n' +
    '                    <td>\n' +
    '                        <label>Position:</label>\n' +
    '                        <input type="text" class="large-text"  name="ourpeoples['+time+'][position]" value="">\n' +
    '                    </td>\n' +
    '                    <td>\n' +
    '                        <a href="#" name="remove-our-people">-Remove</a>\n' +
    '                    </td>\n' +
    '                </tr>';

        $("#our-people-body").append(data);
    });

    $("a[name=remove-our-people]").live("click", function(e){
        e.preventDefault();

        $(this).parent().parent().remove();
    });

});