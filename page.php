<?php
get_header("general");

while (have_posts()):
    the_post();
?>

    <!-- LIGHT SECTION -->
    <section class="lightSection clearfix pageHeader">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb float-right">
                        <li>
                            <a href="<?php bloginfo("url"); ?>">Home</a>
                        </li>
                        <li class="active"><?php the_title(); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- MAIN CONTENT SECTION -->
    <section class="mainContent clearfix genricContent">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>

<?php
    endwhile;
get_footer("general");
?>