<?php
get_header("general");
?>
    <!-- LIGHT SECTION -->
    <section class="lightSection clearfix pageHeader">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb float-right">
                        <li>
                            <a href="<?php bloginfo("url"); ?>">Home</a>
                        </li>
                        <li class="active"><?php the_title(); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- MAIN CONTENT SECTION -->
    <section class="mainContent clearfix blogPage">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-sm-12 ">
                    <div class="row">
                        <?php
                         while (have_posts()){
                             the_post();
                        ?>
                            <div class="col-sm-12">
                            <div class="thumbnail">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail(array(825, 388), array("class" => "mw-100"))?>
                                </a>
                                <div class="caption">
                                    <div class="row">
                                        <div class="col-md-3 order-md-12">
                                            <h5>Details</h5>
                                            <ul class="list-unstyled">
                                                <li><a href="<?php the_author_link(); ?>"><i class="fa fa-user" aria-hidden="true"></i><?php the_author(); ?></a></li>
                                                <li><i class="fa fa-calendar" aria-hidden="true"></i><?php the_date("M d, Y");?></li>
                                                <li><?php the_tags(); ?></li>
                                                <li><a href="<?php the_permalink();?>#commentform"><i class="fa fa-envelope" aria-hidden="true"></i><?php comments_number(); ?> comments</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-9 order-md-1">
                                            <h3><a href="blog-single-right-sidebar.html"><?php the_title(); ?></a> </h3>
                                            <p><?php the_excerpt(); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }

                        ?>
                    </div>

                    <!-- Pagination -->
                    <?php
                    global $wp_query;

                    $big = 999999999; // need an unlikely integer

                    $paginations =  paginate_links( array(
	                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	                    'format' => '?paged=%#%',
	                    'current' => max( 1, get_query_var('paged') ),
	                    'total' => $wp_query->max_num_pages,
                        'type' => 'array',
                        'prev_text' => '«',
                        'next_text' => '»'
                    ) );

                    if($paginations){
                    ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-left">
                                <ul class="pagination">
                                    <?php foreach ($paginations as $pagination){ ?>
                                        <?php
                                        if(strpos($pagination, "current") !== false){
                                            echo "<li class='active'><a href='javascript:void(0)'>".$pagination."</a></li>";
                                        }else{
                                            echo "<li>".$pagination."</li>";
                                        }
                                        ?>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>

<?php
get_footer("general");