<div class="col-lg-3 col-md-12 sideBar">
	<form action="<?php bloginfo("url");?>" method="get">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon2" name="s">
			<button class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></button>
		</div>
	</form>
	<div class="panel panel-default">
		<div class="panel-heading">categories</div>
		<div class="panel-body">
			<ul class="list-unstyle sidebar-list">
				<?php
				$categories = get_categories();
				foreach($categories as $category){
					?>
					<li>
						<a href="<?php echo get_category_link($category->term_id)?>">
							<i class="fa fa-caret-right" aria-hidden="true"></i>
							<?php echo $category->name; ?> <span>(<?php echo $category->count; ?>)</span>
						</a>
					</li>
					<?php
				}
				?>
			</ul>
		</div>
	</div>
	<div class="panel panel-default recentBlogPosts">
		<div class="panel-heading">recent posts</div>
		<div class="panel-body">
			<?php
			$recentlyposts = get_posts(array(
				"numberposts" => 5,
			));

			foreach ($recentlyposts as $recentlypost){
			?>

			<div class="media">
				<a class="media-left" href="<?php echo get_the_permalink($recentlypost); ?>">
					<img width="70" class="media-object" src="<?php echo get_the_post_thumbnail_url($recentlypost, array(70, 70))?>" alt="<?php echo get_the_title($recentlypost); ?>">
				</a>
				<div class="media-body">
					<h4 class="media-heading"><a href="<?php the_permalink($recentlypost); ?>"><?php echo get_the_title($recentlypost); ?></a></h4>
					<p><i class="fa fa-calendar" aria-hidden="true"></i><?php echo get_the_date("M d, Y", $recentlypost);?></p>
				</div>
			</div>

			<?php
			}
			?>
		</div>
	</div>
</div>