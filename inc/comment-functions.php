<?php
function themecourse_list_comment_open($comment, $args, $depth)
{
?>
	<div class="media flex-wrap">
		<a class="media-left" href="<?php echo get_comment_author_url(); ?>">
			<?php echo get_avatar( $comment, 60 ); ?>
		</a>
		<div class="media-body">
			<h4 class="media-heading"><?php echo get_comment_author(); ?></h4>
			<h4><span><i class="fa fa-calendar" aria-hidden="true"></i><?php echo get_comment_date("M d, Y"); ?></span></h4>
			<?php echo get_comment_text(); ?>
			<p>
			<?php
			comment_reply_link(array(
				'add_below' => 'div-comment',
				'depth'     => $depth,
				'max_depth' => $args['max_depth'],
			));
			?>
			</p>

<?php
}

function themecourse_list_comment_close($comment, $args, $depth)
{
    ?>
        </div>
    </div>
    <?php
}