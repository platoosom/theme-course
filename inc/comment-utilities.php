<?php
/** Run when creating comment item */
function themecourse_list_comment_open($comment, $args, $depth)
{
	?>
	<div class="media flex-wrap">
		<a class="media-left" href="<?php comment_author_url();?>">
			<?php echo get_avatar($comment, 60); ?>
		</a>
		<div class="media-body">
			<h4 class="media-heading"><?php echo comment_author(); ?></h4>
			<h4><span><i class="fa fa-calendar" aria-hidden="true"></i><?php comment_date("M d, Y"); ?></span></h4>
			<?php comment_text(); ?>

			<?php
			comment_reply_link(array(
				"add_below" => "div-comment",
				"depth" => $depth,
				"max_depth" => $args["max_depth"],
			));
	?>
<?php
}

/** Run when closing comment item */
function themecourse_list_comment_close($comment, $args, $depth)
{
	?>
		</div>
	</div>
	<?php
}

/** Add div class row under form */
add_action("comment_form_top", "themecourse_add_div");

function themecourse_add_div()
{
	echo "<div class='row' ";
}

/** Close div class row */
add_filter("comment_form_submit_field", "themecourse_add_end_div", 10, 2);

function themecourse_add_end_div($submit_field, $args)
{
	$submit_field = "</div>".$submit_field;

	return $submit_field;
}

