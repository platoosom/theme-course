<?php
add_action("comment_form_top", "themecourse_comment_form_top");

function themecourse_comment_form_top()
{
    echo "		
            <div class=\"row\">";
}

/** Modify comment field */
add_filter("comment_form_field_comment", "themecourse_modify_comment_field");

function themecourse_modify_comment_field($field)
{
    ob_start();
    ?>
    <div class="col-12">
        <div class="form-group">
            <textarea class="form-control" rows="3" placeholder="Comment" name="comment"></textarea>
        </div>
    </div>
    <?php
    return ob_get_clean();
}

/** Modify author field */
add_filter("comment_form_field_author", "themecourse_modify_author_field");

function themecourse_modify_author_field($field)
{
    ob_start();
    ?>
    <div class="col-sm-6 col-12">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Name" name="author" required="required">
        </div>
    </div>
    <?php
    return ob_get_clean();
}

/** Modify email field */
add_filter("comment_form_field_email", "themecourse_modify_email_field");

function themecourse_modify_email_field($field)
{
    ob_start();
    ?>
    <div class="col-sm-6 col-12">
        <div class="form-group">
            <input type="text" class="form-control"  placeholder="Email" name="email" required="required">
        </div>
    </div>
    <?php
    return ob_get_clean();
}

/** Modify url field */
add_filter("comment_form_field_url", "themecourse_modify_url_field");

function themecourse_modify_url_field($field)
{
    ob_start();
    ?>
    <div class="col-12">
        <div class="form-group">
            <input type="text" class="form-control" name="url">
        </div>
    </div>
    <?php
    return ob_get_clean();
}

/** Modify cookies field */
add_filter("comment_form_field_cookies", "themecourse_modify_cookies_field");

function themecourse_modify_cookies_field($field)
{
    ob_start();
    ?>
    <div class="col-12">
        <?php echo $field; ?>
    </div>
    <?php
    return ob_get_clean();
}


add_filter("comment_form_submit_field", "themecourse_comment_form_submit_field", 10, 2);

function themecourse_comment_form_submit_field($submit_field, $args)
{
    $submit_field  = '</div>'. $submit_field;

    return $submit_field;
}

/** Modify logined user link */
add_filter("comment_form_logged_in", "themecourse_comment_form_logged_in", 10, 3);

function themecourse_comment_form_logged_in($arg, $commenter, $user_identity )
{
    $arg = str_replace("<a", "<a class='login-link' ", $arg);
    $arg = "<div class='col-12'>".$arg."</div>";
    return $arg;
}

