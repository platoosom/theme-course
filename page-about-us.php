<?php
get_header("general");

while (have_posts()):
	the_post();
?>
    <!-- LIGHT SECTION -->
    <section class="lightSection clearfix pageHeader">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title">
                        <h2>About Us</h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb float-right">
                        <li>
                            <a href="<?php bloginfo("url"); ?>">Home</a>
                        </li>
                        <li class="active">About Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- MAIN CONTENT SECTION -->
    <section class="mainContent clearfix aboutUsInfo">
        <div class="container">
            <div class="page-header">
                <h3>Suspendisse suscipit vestibulum dignissim</h3>
            </div>
            <div class="row">
                <div class="col-md-6 order-sm-12">
                    <img src="<?php bloginfo("template_url"); ?>/img/about-us/title-img.jpg" alt="about-us-img">
                </div>
                <div class="col-md-6 order-sm-1">
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p>Mauris lobortis augue ex, ut faucibus nisi mollis ac. Sed volutpat scelerisque ex ut ullamcorper. Cras at velit quis sapien dapibus laoreet a id odio. Sed sit amet accumsan ante, eu congue metus. Aenean eros tortor, cursus quis feugiat sed, vestibulum vel purus. Etiam aliquam turpis quis blandit finibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porttitor placerat lectus, facilisis ornare leo tincidunt vel. Duis rutrum felis felis, eget malesuada massa tincidunt a.</p>
                    <ul class="unorder-list lists">
                        <li>Neque porro quisquam est,</li>
                        <li>qui dolorem ipsum quia dolor sit amet, </li>
                        <li>consectetur, adipisci velit, sed quia</li>
                        <li>non numquam eius modi tempora incidunt </li>
                        <li>ut labore et dolore magnam aliquam</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- DARK SECTION -->
    <section class="darkSection clearfix">
        <div class="container">
            <h3>Our Store Locations</h3>
            <div class="row">
                <?php
                $locations = get_post_meta($post->ID, "store_locations", true);
                foreach ($locations as $location){
                ?>
                    <div class="col-md-3">
                    <div class="thumbnail">
                        <div class="caption">
                            <h5><?php echo $location["title"]; ?></h5>
                            <address>
                                <?php echo nl2br($location["location"]); ?>
                            </address>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
    </section>

    <!-- WHITE SECTION -->
    <section class="whiteSection clearfix aboutPeople">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Our Peoples</h3>
                </div>
                <?php
                $ourpeoples = get_post_meta($post->ID, "ourpeoples", true);

                foreach ($ourpeoples as $ourpeople){
                ?>
                    <div class="col-md-3">
                    <div class="thumbnail">
                        <img src="<?php echo $ourpeople["pic"]; ?>" alt="people-image">
                        <div class="caption">
                            <h5><?php echo $ourpeople["name"]; ?></h5>
                            <p><?php echo $ourpeople["position"]; ?></p>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>

            </div>
        </div>
    </section>

<?php
endwhile;

get_footer("general");
?>