<div class="commentsArea">
	<h3><?php echo get_comments_number(); ?> Comments</h3>
	<?php
	wp_list_comments(array(
		"callback" => "themecourse_list_comment_open",
		"end-callback" => "themecourse_list_comment_close",
		"type" => "comment",
		"style" => "div",
	));
	?>
</div>