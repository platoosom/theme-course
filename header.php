<!DOCTYPE html>
<html lang="en">
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_bloginfo("name")." - ".get_bloginfo("description"); ?></title>

    <?php wp_head(); ?>

</head>

<body class="body-wrapper version1">

<!-- Preloader -->
<?php
// get_template_part("template-part/preloader");
?>

<div class="main-wrapper">

    <!-- HEADER -->
    <div class="header clearfix">

        <!-- TOPBAR -->
        <?php
        get_template_part("template-part/topbar");
        ?>
        <!-- NAVBAR -->
        <?php
        get_template_part("template-part/navbar");
        ?>

    </div>

    <!-- BANNER -->
    <?php
    get_template_part("template-part/banner")
    ?>