<?php

/** Require inc files */
require_once "inc/comment-utilities.php";

/** Theme supports */
add_theme_support("post-thumbnails");

/** Add custom image size */
add_image_size( 'image220x180', 220, 0, true );

/** Enqueue stylesheet files */
add_action("wp_enqueue_scripts", "themecourse_enqueue_styles");

function themecourse_enqueue_styles()
{
    /** <!-- PLUGINS CSS STYLE --> */
    wp_enqueue_style("jquery-ui", get_bloginfo("template_url")."/plugins/jquery-ui/jquery-ui.css");
    wp_enqueue_style("bootstrap", get_bloginfo("template_url")."/plugins/bootstrap/css/bootstrap.min.css");
    wp_enqueue_style("font-awesome", get_bloginfo("template_url")."/plugins/font-awesome/css/font-awesome.min.css");
    wp_enqueue_style("select_option1", get_bloginfo("template_url")."/plugins/selectbox/select_option1.css");
    wp_enqueue_style("jquery.fancybox", get_bloginfo("template_url")."/plugins/fancybox/jquery.fancybox.min.css");
    wp_enqueue_style("iziToast", get_bloginfo("template_url")."/plugins/iziToast/css/iziToast.css");
    wp_enqueue_style("settings", get_bloginfo("template_url")."/plugins/rs-plugin/css/settings.css");
    wp_enqueue_style("prism", get_bloginfo("template_url")."/plugins/prismjs/prism.css");
    wp_enqueue_style("slick", get_bloginfo("template_url")."/plugins/slick/slick.css");
    wp_enqueue_style("slick-theme", get_bloginfo("template_url")."/plugins/slick/slick-theme.css");
    wp_enqueue_style("prism", get_bloginfo("template_url")."/plugins/prismjs/prism.css");

    /** <!-- CUSTOM CSS --> */
    wp_enqueue_style("style", get_bloginfo("template_url")."/css/style.css");
    wp_enqueue_style("default", get_bloginfo("template_url")."/css/default.css" );

}

/** Enqueue script files */
add_action("wp_enqueue_scripts", "themecourse_enqueue_scripts");

function themecourse_enqueue_scripts()
{
	wp_enqueue_script("jquery-ui", get_bloginfo("template_url")."/plugins/jquery-ui/jquery-ui.js", array("jquery"), false, true);
	wp_enqueue_script("popper", get_bloginfo("template_url")."/plugins/bootstrap/js/popper.min.js", array(), false, true);
	wp_enqueue_script("bootstrap", get_bloginfo("template_url")."/plugins/bootstrap/js/bootstrap.min.js", array(), false, true);
	wp_enqueue_script("jquery.themepunch.tools", get_bloginfo("template_url")."/plugins/rs-plugin/js/jquery.themepunch.tools.min.js", array(), false, true);
	wp_enqueue_script("jquery.themepunch.revolution", get_bloginfo("template_url")."/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js", array(), false, true);
	wp_enqueue_script("slick", get_bloginfo("template_url")."/plugins/slick/slick.js", array(), false, true);
	wp_enqueue_script("jquery.fancybox", get_bloginfo("template_url")."/plugins/fancybox/jquery.fancybox.min.js", array("jquery"), false, true);
	wp_enqueue_script("iziToast", get_bloginfo("template_url")."/plugins/iziToast/js/iziToast.js", array(), false, true);
	wp_enqueue_script("prism", get_bloginfo("template_url")."/plugins/prismjs/prism.js", array(), false, true);
	wp_enqueue_script("jquery.selectbox", get_bloginfo("template_url")."/plugins/selectbox/jquery.selectbox-0.1.3.min.js", array("jquery"), "0.1.3", true);
	wp_enqueue_script("jquery.syotimer", get_bloginfo("template_url")."/plugins/countdown/jquery.syotimer.js", array("jquery"), false, true);
	wp_enqueue_script("velocity", get_bloginfo("template_url")."/plugins/velocity/velocity.min.js", array(), false, true);
	wp_enqueue_script("custom", get_bloginfo("template_url")."/js/custom.js", array(), false, true);

	wp_enqueue_script("jcrop");
	wp_enqueue_script("jquery");
}

add_action("wp_head", "themecourse_add_extrainformation");

function themecourse_add_extrainformation()
{
	?>
	<!-- Icons -->
	<link rel="shortcut icon" href="<?php bloginfo("template_url")?>/img/favicon.png">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?php
}

add_filter("login_headertitle", "themecourse_change_headertitle");

function themecourse_change_headertitle()
{
	return "HI SUN";
}

/** Create socail option page */
add_action("admin_menu", "themecourse_admin_menu");

function themecourse_admin_menu()
{
    add_menu_page("Social links", "Social Links", "manage_options", "social_links", "themecourse_social_links_conten");
}

function themecourse_social_links_conten()
{
    if(wp_verify_nonce($_POST["_wpnonce"], "social_links")){
        update_option("sociallinks", $_POST["sociallinks"]);
        $updated = true;
    }

	$options = array(
        "twitter" => "",
        "facebook" => "",
    );

    if(get_option("sociallinks")){
        $options = get_option("sociallinks");
    }
    ?>
    <div class="wrap">
        <h1>Social Links</h1>

        <?php if(isset($updated) && $updated == true){?>
        <div id="setting-error-settings_updated" class="updated settings-error notice">
            <p><strong>บันทึกการตั้งค่าแล้ว</strong></p>
        </div>
        <?php }?>

        <form method="post" action="" novalidate="novalidate">
            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row">
                        <label for="sociallinks_twitter">Twitter</label></th>
                    <td>
                        <input name="sociallinks[twitter]" type="text" id="sociallinks_twitter" value="<?php echo $options["twitter"]; ?>" class="regular-text">
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="sociallinks_twitter">Facebook</label></th>
                    <td>
                        <input name="sociallinks[facebook]" type="text" id="sociallinks_facebook" value="<?php echo $options["facebook"]; ?>" class="regular-text">
                    </td>
                </tr>
            </table>
            <p class="submit">
                <input type="submit" name="submit" id="submit" class="button button-primary" value="บันทึกการเปลี่ยนแปลง">
                <?php wp_nonce_field("social_links")?>
            </p>
        </form>
    </div>
<?php
}


/** Register menu location  */
add_action("after_setup_theme", "themecourse_register_menu_location");

function themecourse_register_menu_location()
{
    register_nav_menus(array(
            "primary" => "Primary menu on top",
            "footer1" => "Footer 1 location",
            "footer2" => "Footer 2",
            "footer3" => "Footer 3",
    ));
}

/** Add li classes */
add_filter("nav_menu_css_class", "themecourse_add_nave_menu_css_class", 10, 4);

function themecourse_add_nave_menu_css_class($classes, $item, $args, $depth)
{
	if($depth == 0){
		$classes[] = "nav-item dropdown";
	}

	/** Mega menu */
	if($item->description == "menutypemega"){
		$classes[] = "megaDropMenu";
	}

	/** Mega child */
	if($item->description == "menutypemegachild" || $item->description == "megamenutypeimage" ){
		$classes[] = "col-md-3 col-12";
	}

	/** Mega group item */
	if($item->description == "menutypemegagroup"){
		$classes[] = "listHeading";
	}

	return $classes;
}

/** Add class to submenu */
add_filter("nav_menu_submenu_css_class", "themecourse_modify_submenu", 10, 3);

function themecourse_modify_submenu($classes, $args, $depth)
{
	if($depth == 0){
		$classes[] = "dropdown-menu dropdown-menu-left dropdown-menu row";
	}
	if($depth == 1){
		$classes[] = "list-unstyled";
	}
	return $classes;
}


/** Add class to anchor */
add_filter( 'nav_menu_link_attributes', 'themecourse_add_specific_menu_location_atts', 10, 4 );

function themecourse_add_specific_menu_location_atts( $atts, $item, $args, $depth ) {

	// check if the item is in the primary menu
	if( $args->theme_location == 'primary' && $depth == 0 ) {
		// add the desired attributes:
		$atts['class'] = 'dropdown-toggle nav-link';
	}
	return $atts;
}


/** Remove unwanted thing (inside li)  */
add_filter("walker_nav_menu_start_el", "themecourse_remove_megamenu_child", 10, 4);

function themecourse_remove_megamenu_child($item_output, $item, $depth, $args)
{
	if($args->theme_location == "primary"){
		if($item->description == "menutypemegachild"){
			$item_output = "";
		}

		if($item->description == "menutypemegagroup"){
			$item_output = $item->post_title;
		}

		if($item->description == "megamenutypeimage"){
			$item_output = "<a href='#' class='menu-photo'><img src='{$item->url}' /></a>";
		}


	}
	return $item_output;
}


/** Register banner custom posttype */
add_action("init", "themecourse_register_banner_posttype");

function themecourse_register_banner_posttype()
{
    register_post_type("banner", array(
        "label" => "Banner",
        "supports" => array("title", "editor", "thumbnail", "custom-fields"),
        "rewrite" => array("slug" => "banner"),
        "public" => true,
    ));
}


/** Register brand custom posttype */
add_action("init", "themecourse_register_brand_posttype");

function themecourse_register_brand_posttype()
{
	register_post_type("brand", array(
		"label" => "Brand",
		"supports" => array("title","thumbnail"),
		"rewrite" => array("slug" => "brand"),
		"public" => true,
	));
}

/** Register Sidebar */
add_action("init", "themecourse_register_singlesidebar");

function themecourse_register_singlesidebar()
{
    register_sidebar(array(
        "name" => "Single Page Sidebar",
        "id" => "singlesidebar",
        'before_widget' => '<div id="%1$s" class="panel panel-default %2$s">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<div class="panel-heading">',
        'after_title'   => '</div><div class="panel-body">'
    ));
}

/** Add metabox to about-us page */
add_action("add_meta_boxes", "themecourse_register_our_location_metabox");

function themecourse_register_our_location_metabox()
{
    global $post;
    if($post->post_name == "about-us"){
        add_meta_box(
        "about-us-our-location",
            "Our Location",
            "themecourse_our_location_content",
            "page"
        );
    }

}

function themecourse_our_location_content()
{
    global  $post;

	?>
    <table class="form-table" id="our-store-location">
        <tbody id="our-store-location-body">
        <tr>
            <td colspan="3"><a href="" id="add-our-store-location">+</a></td>
        </tr>

        <?php
        $ourstorelocations = get_post_meta($post->ID, "store_locations", true);

        foreach ($ourstorelocations as $key => $ourstorelocation){
        ?>
            <tr>
                <td>
                    <label>Title:</label>
                    <input class="large-text" type="text" name="ourstorelocation[<?php echo $key; ?>][title]" value="<?php echo $ourstorelocation["title"]; ?>">
                </td>
                <td>
                    <label>Location:</label>
                    <textarea class="large-text" rows="3"  name="ourstorelocation[<?php echo $key; ?>][location]"><?php echo $ourstorelocation["location"]; ?></textarea>
                </td>
                <td>
                    <a href="#" name="remove-our-store-location">-Remove</a>
                </td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
    <?php
}

/** Save our store location to database */
add_action("save_post", "themecourse_save_our_store_location");

function themecourse_save_our_store_location($post_id)
{
    $post = get_post($post_id);

    if($post->post_name == "about-us"){
        update_post_meta($post_id, "store_locations", $_POST["ourstorelocation"]);
    }
}

/** Register admin js */
add_action("admin_enqueue_scripts", "themecourse_admin_script");

function themecourse_admin_script()
{
    wp_enqueue_script("themecourse-admin-script", get_bloginfo("template_url")."/js/theme-admin-script.js");
}


/** Register our customer metabox */
add_action("add_meta_boxes", "themecourse_register_our_people_metabox");

function themecourse_register_our_people_metabox()
{
	global $post;
	if($post->post_name == "about-us"){
		add_meta_box(
			"about-us-our-people",
			"Our People",
			"themecourse_our_people_content",
			"page"
		);
	}

}


function themecourse_our_people_content()
{
	global  $post;

	?>
    <table class="form-table">
        <tbody id="our-people-body">
        <tr>
            <td colspan="3"><a href="" id="add-our-people">+</a></td>
        </tr>

		<?php
		$ourpeoples = get_post_meta($post->ID, "ourpeoples", true);

		if(is_array($ourpeoples)){
            foreach ($ourpeoples as $key => $ourpeople){
                ?>
                <tr>
                    <td>
                        <label>Pic:</label>
                        <input class="large-text" type="text" name="ourpeoples[<?php echo $key; ?>][pic]" value="<?php echo $ourpeople["pic"]; ?>">
                    </td>
                    <td>
                        <label>Name:</label>
                        <input type="text" class="large-text"  name="ourpeoples[<?php echo $key; ?>][name]" value="<?php echo $ourpeople["name"]; ?>">
                    </td>
                    <td>
                        <label>Position:</label>
                        <input type="text" class="large-text"  name="ourpeoples[<?php echo $key; ?>][position]" value="<?php echo $ourpeople["position"]; ?>">
                    </td>
                    <td>
                        <a href="#" name="remove-our-people">-Remove</a>
                    </td>
                </tr>
                <?php
            }
		}
		?>
        </tbody>
    </table>
	<?php
}

/** Save our people to database */
add_action("save_post", "themecourse_save_our_peoples");

function themecourse_save_our_peoples($post_id)
{
	$post = get_post($post_id);

	if($post->post_name == "about-us"){
		update_post_meta($post_id, "ourpeoples", $_POST["ourpeoples"]);
	}
}