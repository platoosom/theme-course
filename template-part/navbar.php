<!-- NAVBAR -->
<nav class="navbar navbar-main navbar-default navbar-expand-md" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->

        <a class="navbar-brand" href="<?php bloginfo("url");?>">
            <img src="<?php bloginfo("template_url");?>/img/logo.png">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-ex1-collapse" aria-controls="navbar-ex1-collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span>
        </button>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <?php
        wp_nav_menu(array(
                "theme_location" => "primary",
                "container_class" => "collapse navbar-collapse navbar-ex1-collapse",
                "menu_class" => "nav navbar-nav ml-auto",
        ));
        ?>

        <div class="version2">
            <div class="dropdown cart-dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle shop-cart" data-toggle="dropdown">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="badge">3</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>Item(s) in your cart</li>
                    <li>
                        <a href="single-product.html">
                            <div class="media">
                                <img class="media-left media-object" src="<?php bloginfo("template_url")?>/img/home/cart-items/cart-item-01.jpg" alt="cart-Image">
                                <div class="media-body">
                                    <h5 class="media-heading">INCIDIDUNT UT <br><span>2 X $199</span></h5>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="single-product.html">
                            <div class="media">
                                <img class="media-left media-object" src="<?php bloginfo("template_url")?>/img/home/cart-items/cart-item-01.jpg" alt="cart-Image">
                                <div class="media-body">
                                    <h5 class="media-heading">INCIDIDUNT UT <br><span>2 X $199</span></h5>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-default" onclick="location.href='cart-page.html';">Shopping Cart</button>
                            <button type="button" class="btn btn-default" onclick="location.href='checkout-step-1.html';">Checkout</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>