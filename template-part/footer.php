<!-- FOOTER -->
<div class="footer clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-12">
                <div class="footerLink">
                    <h5>Accessories</h5>
	                <?php
	                wp_nav_menu(array(
		                "theme_location" => "footer1",
		                "container" => false,
		                "menu_class" => "list-unstyled",
	                ));
	                ?>
                </div>
            </div>
            <div class="col-md-2 col-12">
                <div class="footerLink">
                    <h5>BRANDS</h5>
	                <?php
	                wp_nav_menu(array(
		                "theme_location" => "footer2",
		                "container" => false,
		                "menu_class" => "list-unstyled",
	                ));
	                ?>
                </div>
            </div>
            <div class="col-md-2 col-12">
                <div class="footerLink">
                    <h5>Accessories</h5>
	                <?php
	                wp_nav_menu(array(
		                "theme_location" => "footer3",
		                "container" => false,
		                "menu_class" => "list-unstyled",
	                ));
	                ?>
                </div>
            </div>
            <div class="col-md-2 col-12">
                <div class="footerLink">
                    <h5>Get in Touch</h5>
                    <ul class="list-unstyled">
                        <li>Call us at (089)-063-8436</li>
                        <li><a href="mailto:platoosom@gmail.com">platoosom@gmail.com</a></li>
                    </ul>
                    <ul class="list-inline">
	                    <?php
	                    $options = array(
		                    "twitter" => "",
		                    "facebook" => "",
	                    );

	                    if(get_option("sociallinks")){
		                    $options = get_option("sociallinks");
	                    }
	                    ?>
                        <li><a href="<?php echo $options["twitter"]; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo $options["facebook"]; ?>"  target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="newsletter clearfix">
                    <h4>Newsletter</h4>
                    <h3>Sign up now</h3>
                    <p>Enter your email address and get notified about new products. We hate spam!</p>
                    <?php echo do_shortcode('[email-subscribers-form id="2"]'); ?>

                </div>
            </div>
        </div>
    </div>
</div>