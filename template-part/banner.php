<!-- BANNER -->
<div class="bannercontainer bannerV1">
    <div class="fullscreenbanner-container">
        <div class="fullscreenbanner">
            <?php
            $banners = get_posts(array(
                    "post_type" => "banner"
            ));
            $banners->found_posts
            ?>
            <ul>
                <?php
                foreach($banners as $banner){
                ?>
                    <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                    <img src="<?php bloginfo("template_url")?>/img/home/banner-slider/slider-bg.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <div class="slider-caption slider-captionV1 container">

                        <div class="tp-caption rs-caption-1 sft start"
                             data-hoffset="0"
                             data-x="700"
                             data-y="0"
                             data-speed="800"
                             data-start="1500"
                             data-easing="Back.easeInOut"
                             data-endspeed="300" >
                            <img src="<?php echo get_the_post_thumbnail_url($banner, "full"); ?>" alt="slider-image" style="width: auto; height: auto;">
                        </div>

                        <div class="tp-caption rs-caption-2 sft"
                             data-hoffset="0"
                             data-y="100"
                             data-x="[15,15,42,15]"
                             data-speed="800"
                             data-start="2000"
                             data-easing="Back.easeInOut"
                             data-endspeed="300">
                            <?php echo $banner->post_title; ?>
                        </div>

                        <div class="tp-caption rs-caption-3 sft"
                             data-hoffset="0"
                             data-y="175"
                             data-x="[15,15,42,15]"
                             data-speed="1000"
                             data-start="3000"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="off">
                            <?php echo get_post_meta($banner->ID, "price", true)?><br>
                            <small><?php echo $banner->post_content; ?></small>
                        </div>
                        <div class="tp-caption rs-caption-4 sft"
                             data-hoffset="0"
                             data-y="310"
                             data-x="[15,15,42,15]"
                             data-speed="800"
                             data-start="3500"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="off">
                            <span class="page-scroll"><a href="<?php echo get_post_meta($banner->ID, "buynow-url", true)?>" class="btn primary-btn">Buy Now<i class="fa fa-chevron-right"></i></a></span>
                        </div>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>
</div>
