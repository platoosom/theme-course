<!-- COPY RIGHT -->
<div class="copyRight clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-12">
                <p>&copy; 2019 Copyright Theme Couse dot com</p>
            </div>
            <div class="col-md-5 col-12">
                <ul class="list-inline">
                    <li><img src="<?php bloginfo("template_url")?>/img/home/footer/card1.png"></li>
                    <li><img src="<?php bloginfo("template_url")?>/img/home/footer/card2.png"></li>
                    <li><img src="<?php bloginfo("template_url")?>/img/home/footer/card3.png"></li>
                    <li><img src="<?php bloginfo("template_url")?>/img/home/footer/card4.png"></li>
                </ul>
            </div>
        </div>
    </div>
</div>
