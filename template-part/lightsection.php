<!-- LIGHT SECTION -->
<section class="lightSection clearfix">
    <div class="container">
        <div class="owl-carousel partnersLogoSlider">

	        <?php
	        $brands = get_posts( array(
		        "post_type" => "brand",
		        'orderby'   => 'date',
		        'order'     => 'ASC',
	        ) );

	        foreach($brands as $brand){
		        ?>
                <div class="slide">
                    <div class="partnersLogo clearfix">
                        <img src="<?php echo get_the_post_thumbnail_url($brand->ID, "full"); ?>" alt="partner-img">
                    </div>
                </div>
		        <?php
	        }
            ?>
        </div>
    </div>
</section>
