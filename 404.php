<?php
get_header("404");
?>
	<!-- MAIN CONTENT SECTION -->
	<section class="mainContent clearfix notFound">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 col-12">
					<a href="index.html" class="navbar-brand pageLogo"><img src="<?php bloginfo("template_url"); ?>/img/logo.png" alt="logo"></a>
					<h1>404</h1>
					<h2>Oops! Page Not Found</h2>
					<a class="btn btn-default" href="<?php bloginfo("url"); ?>" role="button">Go Home</a>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer("404");
